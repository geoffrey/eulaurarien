#!/usr/bin/env python3
# pylint: disable=C0103

"""
Filter out invalid domain names
"""

import database
import argparse
import sys

if __name__ == '__main__':

    # Parsing arguments
    parser = argparse.ArgumentParser(
        description="Filter out invalid domain name/ip addresses from a list.")
    parser.add_argument(
        '-i', '--input', type=argparse.FileType('r'), default=sys.stdin,
        help="Input file, one element per line")
    parser.add_argument(
        '-o', '--output', type=argparse.FileType('w'), default=sys.stdout,
        help="Output file, one element per line")
    parser.add_argument(
        '-d', '--domain', action='store_true',
        help="Can be domain name")
    parser.add_argument(
        '-4', '--ip4', action='store_true',
        help="Can be IP4")
    args = parser.parse_args()

    for line in args.input:
        line = line.rstrip().lower()
        if (args.domain and database.Database.validate_domain(line)) or \
                (args.ip4 and database.Database.validate_ip4address(line)):
            print(line, file=args.output)
