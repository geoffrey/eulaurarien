#!/usr/bin/env python3

import argparse
import database
import time
import os

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description="Database operations")
    parser.add_argument(
        "-i", "--initialize", action="store_true", help="Reconstruct the whole database"
    )
    parser.add_argument(
        "-p", "--prune", action="store_true", help="Remove old entries from database"
    )
    parser.add_argument(
        "-b",
        "--prune-base",
        action="store_true",
        help="With --prune, only prune base rules "
        "(the ones added by ./feed_rules.py)",
    )
    parser.add_argument(
        "-s",
        "--prune-before",
        type=int,
        default=(int(time.time()) - 60 * 60 * 24 * 31 * 6),
        help="With --prune, only rules updated before "
        "this UNIX timestamp will be deleted",
    )
    parser.add_argument(
        "-r",
        "--references",
        action="store_true",
        help="DEBUG: Update the reference count",
    )
    args = parser.parse_args()

    if not args.initialize:
        DB = database.Database()
    else:
        if os.path.isfile(database.Database.PATH):
            os.unlink(database.Database.PATH)
        DB = database.Database()

    DB.enter_step("main")
    if args.prune:
        DB.prune(before=args.prune_before, base_only=args.prune_base)
    if args.references:
        DB.update_references()

    DB.save()
