#!/usr/bin/env bash

function log() {
    echo -e "\033[33m$@\033[0m"
}

oldest="$(cat last_updates/*.txt | sort -n | head -1)"
log "Pruning every record before ${oldest}…"
./db.py --prune --prune-before "$oldest"
