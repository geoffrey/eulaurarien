#!/usr/bin/env bash

function log() {
    echo -e "\033[33m$@\033[0m"
}

# Get all subdomains accessed by each website in the website list

cat websites/*.list | sort -u > temp/all_websites.list
./collect_subdomains.py temp/all_websites.list > temp/subdomains_from_websites.list
sort -u temp/subdomains_from_websites.list > subdomains/from_websites.cache.list
