#!/usr/bin/env bash

# Main script for eulaurarien

[ ! -f .env ] && touch .env

./fetch_resources.sh
./collect_subdomains.sh
./import_rules.sh
./resolve_subdomains.sh
./prune.sh
./export_lists.sh
./generate_index.py

