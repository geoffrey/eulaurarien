#!/usr/bin/env python3

import markdown2

extras = ["header-ids"]

with open("dist/README.md", "r") as fdesc:
    body = markdown2.markdown(fdesc.read(), extras=extras)

output = f"""<!DOCTYPE html>
<html lang="en">
<head>
<title>Geoffrey Frogeye's block list of first-party trackers</title>
<meta charset="utf-8">
<meta name="author" content="Geoffrey 'Frogeye' Preud'homme" />
<link rel="stylesheet" type="text/css" href="markdown7.min.css">
</head>
<body>
{body}
</body>
</html>
"""

with open("dist/index.html", "w") as fdesc:
    fdesc.write(output)
